package com.example.kotlinbasics

fun cityWeather(city:String,low:Int,high:Int,COR:Int ){

    println("City: $city")
    println("Low temperature: $low, high temperature: ${high}" )
    println("Chance of rain: $COR%")
    println()
}