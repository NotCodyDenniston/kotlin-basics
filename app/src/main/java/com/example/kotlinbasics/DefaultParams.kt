package com.example.kotlinbasics

fun displayAlert(operatingSystems: String = "unknown", emailId: String ){

    println("There's a new sign in request on ${operatingSystems} for your email account ${emailId}")
}