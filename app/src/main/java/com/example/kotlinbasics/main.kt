package com.example.kotlinbasics

fun main() {
    printMessages()
    fixedCompileError()
    displaySale()
    getPartyDetails()
    calculateSum(7,12)
    displayAlert( emailId = "abcdsfg@outgmaillook.com")
    cityWeather("Ankara",27,31,82)
    cityWeather("Tokyo",32,36,10)
    cityWeather("Cape Town", 59, 64, 2)
    cityWeather("Guatemala City", 50, 55, 7)
}

